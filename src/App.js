import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Title from "./Components/TitleComp";
import ActivePower from "./Components/ActivePower";
import Graphs from "./Components/GraphsComponent";
import Photo from "./Components/PhotosComponent";
import StatCard from "./Components/StatCard";
import todayyield from "./images/stat icons/dailyEnergy_updated.png";
import solarirradiance from "./images/stat icons/iraadiance.png";
import electricity from "./images/stat icons/total.png";
import co2down from "./images/stat icons/carbonSavings.png";
import { convert, baseUrl } from "./utils/settings";
import "./App.css";
import axios from "axios";

function App() {
  const [load, setLoad] = useState(false);
  const [activePower, setActivePower] = useState(0);
  const [totalYield, setTotalYield] = useState(0);
  const [todayYield, setTodayYield] = useState(0);
  const [carbonSavings, setCarbonSavings] = useState(0);
  const [solarInsolation, setSolarInsolation] = useState(0);
  const [dailyEnergyReadings, setDailyEnergyReadings] = useState(0);
  const [monthlyEnergyReadings, setMonthlyEnergyReadings] = useState(0);
  const [yearlyEnergyReadings, setYearlyEnergyReadings] = useState(0);
  const [hourlyPowerReadings, setHourlyPowerReadings] = useState(0);
  const [hourlyEnergyReadings, setHourlyEnergyReadings] = useState(0);
  const [totalCapacity, setTotalCapacity] = useState(0);
  const [settings, setSettings] = useState({
    dataFetchInterval: 1,
    enableVideoWithoutBreak: false,
    enableVideos: false,
    enableWeather: false,
    photos: []
  });
  const [isOnline, setIsOnline] = useState(false);
  const [isError, setIsError] = useState(null);
  const [statLogos, setStatLogos] = useState({
    dilayEnergy: null,
    totalEnergy: null,
    carbonSavings: null,
    solarIrradiance: null
  });
  let getPlantInfo = async () => {
    await axios
      .get(`${baseUrl}/plant`)
      .then(res => {
        setTotalCapacity(res.data.totalCapacity);
        setIsError(null);
      })
      .catch(err => {
        setIsError(err.message);
        // throw new Error(err);
      });
  };
  let apiSettings = async () => {
    await axios
      .get(`${baseUrl}/settings`)
      .then(res => {
        setSettings({
          ...settings,
          photos: res.data.photos
        });
        setIsError(null);
      })
      .catch(err => {
        setIsError(err.message);
        throw new Error(err.message);
      });
  };
  let apiCall = async () => {
    await axios
      .get(`${baseUrl}/dashboard`)
      .then(res => {
        console.log(res.data)
        setTodayYield(res.data.todayYield);
        setTotalYield(res.data.totalYield);
        setSolarInsolation(res.data.solarInsolation);
        setCarbonSavings(res.data.carbonSavings);
        setActivePower(res.data.activePower);
        setDailyEnergyReadings(res.data.dailyEnergyReadings);
        setMonthlyEnergyReadings(res.data.monthlyEnergyReadings);
        setYearlyEnergyReadings(res.data.yearlyEnergyReadings);
        setHourlyPowerReadings(res.data.hourlyPowerReadings);
        setHourlyEnergyReadings(res.data.hourlyEnergyReadings);
        setIsOnline(true);
        setIsError(null);
      })
      .catch(err => {
        setIsError(err.message);
        // throw new Error(err);
      });
  };
  useEffect(() => {
    apiCall();
    getPlantInfo();
    apiSettings();
    setStatLogos({
      dilayEnergy: todayyield,
      totalEnergy: electricity,
      carbonSavings: co2down,
      solarIrradiance: solarirradiance
    });
    setInterval(() => {
      apiCall();
      apiSettings();
    }, 30 * 1000);
  }, []);
  if(isOnline == false){
    return <div>Loading</div>
  } else {
    return (
      <Container fluid className={"root"}>
        <Row>
          <Col xs={12}>
            <Title></Title>
          </Col>
        </Row>
        <Row>
          <Col xs={12} md={3} lg={3} xl={3}>
            <ActivePower
              power={activePower}
              capacity={totalCapacity}></ActivePower>
          </Col>
          <Col xs={12} md={5} lg={5} xl={6} style={{paddingLeft:'0px'}}>
            <Graphs
              data={[
                dailyEnergyReadings,
                monthlyEnergyReadings,
                yearlyEnergyReadings,
                hourlyEnergyReadings,
                hourlyPowerReadings
              ]}></Graphs>
          </Col>
          <div className={'col-xl-3'} xs={12} md={4} lg={4} xl={4} style={{paddingLeft:'0px'}}>
            <Photo data={settings.photos}></Photo>
          </div>
        </Row>
        <Row>
          <Col md={3} xl={3}>
            <StatCard
              data={convert(todayYield)}
              icon={todayyield}
              title={"Daily Energy"}></StatCard>
          </Col>
          <Col md={3} xl={3} style={{paddingLeft:'0px'}}>
            <StatCard
              data={convert(totalYield)}
              icon={electricity}
              title={"Total Energy"}></StatCard>
          </Col>
          <Col md={3} xl={3} style={{paddingLeft:'0px'}}>
            <StatCard
              data={convert(carbonSavings, "kilo")}
              icon={co2down}
              title={"Carbon Savings"}></StatCard>
          </Col>
          <Col md={3} xl={3} style={{paddingLeft:'0px'}}>
            <StatCard
              data={solarInsolation}
              icon={solarirradiance}
              carbon={carbonSavings}
              type={"irradiace"}
              title='Solar Irradiance'></StatCard>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;
