import React, { Component } from "react";
import moment from "moment";
import { Line, Bar } from "react-chartjs-2";
import { Card } from "react-bootstrap";
import { colors } from "../utils/settings";

export default class GraphsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      BarOptions: {
        maintainAspectRatio: false,
        legend: {
          display: true
        },
        responsive: true,

        tooltips: {
          backgroundColor: "#f5f5f5",
          titleFontColor: "#333",
          bodyFontColor: "#666",
          bodySpacing: 4,
          xPadding: 12,
          mode: "nearest",
          intersect: 0,
          position: "nearest"
        },
        scales: {
          yAxes: [
            {
              "dataset.barPercentage": 1,
              gridLines: {
                drawBorder: false,
                color: "rgba(29,140,248,0.0)",
                zeroLineColor: "transparent"
              },
              ticks: {
                suggestedMin: 0,
                suggestedMax: 2,
                padding: 20,
                fontColor: "#2380f7",
                beginAtZero: true
              }
            }
          ],

          xAxes: [
            {
              "dataset.barPercentage": 1,
              gridLines: {
                drawBorder: false,
                color: "rgba(29,140,248,0.1)",
                zeroLineColor: "transparent"
              },
              ticks: {
                padding: 20,
                suggestedMin: 20,
                fontColor: "#2380f7"
              }
            }
          ]
        }
      },
      loaded: false,
      data: {}
    };
  }

  componentWillMount() {
    console.log(this.props.data);
    let timerId = setTimeout(this.renderGraph(this.props.data), 0);
    setInterval(() => {
      clearInterval(this.timer);
      this.renderGraph(this.props.data);
    }, 60 * 1000);
  }

  renderGraph = x => {
    console.log(x);
    console.log(this.state.count);
    let grpahLabels = [
      "Daily Energy Readings (kWh)",
      "Monthly Energy Readings (MWh)",
      "Yearly Energy Readings (MWh)",
      "Hourly Energy Readings (kWh)",
      "Hourly Power Readings (kW)"
    ];
    let unit = ["dd", "MMM", "YYYY", "ha", "ha"];
    this.timer = setInterval(() => {
      console.log(this.state.count);
      let data = this.processGraph(x[this.state.count], unit[this.state.count]);
      console.log(data);
      this.setState({
        data: {
          labels: data.labels,
          datasets: [
            {
              label: `${grpahLabels[this.state.count]} `,
              fill: true,
              backgroundColor: "rgba(72,72,176,0.2)",
              borderColor: colors.primary,
              borderWidth: 2,
              borderDash: [],
              borderDashOffset: 0.0,
              pointBackgroundColor: "#d048b6",
              pointBorderColor: "rgba(255,255,255,0)",
              pointHoverBackgroundColor: "#d048b6",
              pointBorderWidth: 20,
              pointHoverRadius: 4,
              pointHoverBorderWidth: 15,
              pointRadius: 4,
              data: data.data
            }
          ]
        }
      });
      if (this.state.count === 3 || this.state.count === 4) {
        this.setState({
          data: {
            labels: data.labels,
            datasets: [
              {
                label: `${grpahLabels[this.state.count]}`,
                fill: true,
                backgroundColor: "rgba(72,72,176,0.2)",
                borderColor: "#00d6b4",
                borderWidth: 2,
                borderDash: [],
                borderDashOffset: 0.0,
                pointBackgroundColor: colors.primary,
                pointBorderColor: colors.primary,
                pointHoverBackgroundColor: colors.primary,
                pointBorderWidth: 2,
                pointHoverRadius: 4,
                pointHoverBorderWidth: 15,
                pointRadius: 4,
                data: data.data
              }
            ]
          }
        });
        this.render = () => {
          return (
            <Card body className={"second"}>
              <Line
                data={this.state.data}
                width={400}
                height={220}
                options={this.state.BarOptions}
              />{" "}
            </Card>
          );
        };
      } else {
        this.render = () => {
          return (
            <Card body className={"second"}>
              <Bar
                data={this.state.data}
                width={400}
                height={220}
                options={this.state.BarOptions}
              />{" "}
            </Card>
          );
        };
      }
      let increment = this.state.count;
      increment++;
      this.setState({
        count: increment
      });
      if (this.state.count === grpahLabels.length) {
        this.setState({
          count: 0
        });
      }
    }, 10000);
  };

  processGraph = (data, timeFormat) => {
    console.log(data);
    var max = Math.max(...data.map(r => r.value));
    console.log(max);
    var divisor = 1;
    if (max > 1000 * 1000 * 1000) {
      divisor = 1000 * 1000 * 1000;
    } else if (max > 1000 * 1000) {
      divisor = 1000 * 1000;
    } else if (max > 1000) {
      divisor = 1000;
    }

    return {
      labels: data
        .map(r => moment(r.timestamp * 1000).format(timeFormat))
        .reverse(),
      data: data.map(r => r.value / divisor).reverse(),
      divisor
    };
  };

  divisorToPrefix = divisor => {
    if (divisor === 1) {
      return "";
    } else if (divisor === 1000) {
      return "k";
    } else if (divisor === 1000 * 1000) {
      return "M";
    } else if (divisor === 1000 * 1000 * 1000) {
      return "G";
    }
  };

  render() {
    return (
      <Card body className={"second"}>
        <Bar
          data={this.state.data}
          width={400}
          height={220}
          options={this.state.options}></Bar>
      </Card>
    );
  }
}
