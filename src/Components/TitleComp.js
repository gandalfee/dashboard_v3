import React, { Component } from "react";
import { Card } from "react-bootstrap";
import logo from "../assets/ezgif.com-webp-to-png.png";
import moment from "moment";
import axios from 'axios'
import {baseUrl} from '../utils/settings'

export default class TitleComp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: moment().format("Do MMMM YYYY"),
      day: moment().format("dddd"),
      time: moment().format("hh:mm"),
      name:'',
      location: '',
      logo: '',
      totalCapacity:''
    };
  }

  componentDidMount(props) {
    console.log(props);
    setInterval(() => {
      this.setState({
        date: moment().format("Do MMMM YYYY"),
        day: moment().format("dddd"),
        time: moment().format("hh:mm")
      });
    }, 60 * 1000);

    axios
    .get(`${baseUrl}/plant`)
    .then(res => {
      this.setState({
        name:res.data.name,
        location:res.data.location,
        logo:res.data.logo
      })
    })
    .catch(err => {
      console.log(err);
    });

  }

  render() {
    const wrapper = {
      display: "flex",
      alignItems: "center",
    };
    const image = {
      width:80
    };
    const dateStyle = {
      marginLeft: "auto",
      textAlign: "right",
      height:'36px'
    };
    return (
      <Card body>
        <div style={wrapper}>
          <img src={`${baseUrl}/images/${this.state.logo}`} style={image}></img>
          <div style={{ flexDirection: "column", marginLeft: "15px" }}>
            <h2>{this.state.location}</h2>
            <h4 style={{fontSize:'1rem'}}>{this.state.name}</h4>
          </div>
          <div style={dateStyle}>
            <h4 style={{fontSize:'1rem'}}>{this.state.date}</h4>
            <h4 style={{fontSize:'1rem'}}>{this.state.day}</h4>
          </div>
          <div style={{height:'68px'}}>
            <h1 style={{ fontSize: "4rem", paddingLeft: "15px"}}>
              {this.state.time}
            </h1>
          </div>
        </div>
      </Card>
    );
  }
}
