import React, { useState, useEffect } from "react";
import { Card } from "react-bootstrap";
import Tree from "../images/stat icons/tree_updated.png";

export default function StatCard(props) {
  const [isReady, setIsReady] = React.useState(false);
  const [value, setValue] = React.useState();
  const [carbonSavings, setCarbonSavings] = React.useState();

  React.useEffect(() => {
    setValue(props.data);
    setIsReady(true);
    setCarbonSavings(props.carbon);
  }, [props]);

  const imageWidth = {
    height: 100,
    width: 100
  };

  const info = {
    marginLeft: "auto",
    textAlign: "right"
  };

  const dataTextColor = {
    color: "#2ecc71",
    fontWeight: "400",
    fontSize: "calc(45px + (60 - 45) * ((100vw - 300px) / (2100 - 300)))"
  };

  const unitAndTitle = {
    fontSize: "calc(5px + (24 - 5) * ((100vw - 300px) / (2100 - 300)))",
    textTransform: "uppercase"
  };

  if (isReady === false) {
    return (
      <div>
        <span className='sr-only'> Loading... </span>
      </div>
    );
  } else {
    switch (value.unit) {
      case "Tonnes" || "Kilograms":
        // if (value.data === 0) {
          return (
            <Card className={"statcards"}>
              <div className={"cardStyle"} style={value.data === 0 ? {} : {paddingLeft:'20px', paddingRight:'20px'}}>
                <img
                  src={props.icon}
                  alt=''
                  width={imageWidth.width}
                  height={imageWidth.width}></img>
                <div style={info}>
                  <div style={dataTextColor}>{value.data}</div>
                  <div style={unitAndTitle}>{value.unit}</div>
                  <div style={unitAndTitle}>{props.title}</div>
                </div>
              </div>
            </Card>
          );
      default:
        if (props.type === "irradiace" && props.data === 0.0) {
          return (
            <Card body className={"statcards"}>
              <div className={"cardStyle"}>
                <img
                  src={Tree}
                  alt=''
                  width={imageWidth.width}
                  height={imageWidth.width}></img>
                <div style={info}>
                  <div style={dataTextColor}>
                    {(carbonSavings / 87.5433).toFixed(0)}
                  </div>
                  <div style={unitAndTitle}>Equivalent of</div>
                  <div style={unitAndTitle}>Trees Planted</div>
                </div>
              </div>
            </Card>
          );
        } else {
          return (
            <Card body className={"statcards"}>
              <div className={"cardStyle"}>
                <img
                  src={props.icon}
                  alt=''
                  width={imageWidth.width}
                  height={imageWidth.width}></img>
                <div style={info}>
                  <div style={dataTextColor}>
                    {value.data === undefined
                      ? props.data.toFixed(2)
                      : value.data}
                  </div>
                  <div style={unitAndTitle}>
                    {value.unit === "Kilograms" ? (
                      `${value.unit}`
                    ) : value.unit === undefined ? (
                      <span>
                        {value.unit} watts per
                        <span style={{ textTransform: "lowercase" }}> m²</span>
                      </span>
                    ) : (
                      `${value.unit} - hours`
                    )}
                  </div>
                  <div style={unitAndTitle}>{props.title}</div>
                </div>
              </div>
            </Card>
          );
        }
    }
  }
}
