import React, { Component } from "react";
import JustGage from "justgage";
import { Card } from "react-bootstrap";
import logo from "../images/poweredby_avi.png"
import { convert, convertTokW } from "../utils/settings";

export default class ActivePower extends Component {
  constructor() {
    super();
    this.state = {
      power: 0,
      capacity: 0
    };
  }

  componentWillMount() {
    this.state.power = convert(this.props.power);
    this.state.capacity = convertTokW(this.props.capacity);
    this.state.guage = "";
  }

  componentDidUpdate() {
    this.state.power = convert(this.props.power);
    this.state.capacity = convertTokW(this.props.capacity);
    this.state.guage.refresh(
      this.state.power.data,
      this.state.capacity,
      0,
      this.state.power.unit + "s",
    );
  }


  componentDidMount() {
    this.state.guage = new JustGage({
      id: "g1",
      value: 0,
      min: 0,
      max: this.state.capacity,
      decimals: 2,
      valueFontColor: "#ecf0f1",
      valueFontFamily: "Roboto",
      gaugeColor: "#ecf0f1",
      levelColors: ["#2ecc71"],
      pointer: true,
      pointerOptions: {
        toplength: -15,
        bottomlength: 10,
        bottomwidth: 12,
        color: "#27293d",
        stroke: "#27293d",
        stroke_width: 3,
        stroke_linecap: "round"
      },
      gaugeWidthScale: 0.6,
      counter: true,
      relativeGaugeSize: true,
      donut: true
    });

    this.state.guage.refresh(
      this.state.power.data,
      this.state.capacity,
      0,
      this.state.power.unit + "s"
    );
  }
  render() {
    return (
      <Card body className={'autoHeightWidth'}>
        <div id='g1' style={{paddingTop:'30px'}}></div>
        <img src={logo} alt="" height="65" style={{paddingTop:'20px'}}></img>
      </Card>
    );
  }
}
