import React, { Component } from "react";
import { Card } from "react-bootstrap";
import { baseUrl } from "../utils/settings";

export default class PhotosComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      photos: [],
      currentImage: 0
    };
  }

  componentDidMount() {
    console.log(this.props.data);
    this.setState({ photos: this.props.data });
    this.interval = setInterval(() => {
      this.cycleBackgroudImage();
    }, 8000);
  }

  cycleBackgroudImage = () => {
    let newCurrentImage = 0;
    const { photos, currentImage } = this.state;
    const imagesLength = photos.length;

    if (currentImage !== imagesLength - 1) {
      newCurrentImage = currentImage + 1;
    }

    this.setState({ currentImage: newCurrentImage });
  };

  render() {
    const img = {
      verticalAlign: 'top',
      width:'100%',
      opacity:0,
    }
    return (
      // <Card body className={"third"}>
        // <div
        //   className={"imagesBack"}
        //   style={{
        //     backgroundImage: `url(${baseUrl}/images/${
        //       this.state.photos[this.state.currentImage]
        //     })`
        //   }}></div>
      /* </Card> */
      <div className={'imagesBack'}>
        <img className={'imgDiv'} src={`${baseUrl}/images/${this.state.photos[this.state.currentImage]}`}></img>
      </div>
    );
  }
}
