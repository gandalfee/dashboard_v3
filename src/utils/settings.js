export const baseUrl = "http://localhost:53137/api";
// export const baseUrl = "http://localhost:50794/api";
// export const baseUrl = 'http://localhost:9000/api';
// export const baseUrl = "http://localhost:8500/api";

// export const baseUrl = `http://${window.location.host}/api`;
// export const baseUrl = "http://192.168.100.2/api";

export const convert = (data, units) => {
    var max = data;
    var divisor = 1;
    if (max > 1000 * 1000 * 1000) {
      divisor = 1000 * 1000 * 1000;
    } else if (max > 1000 * 1000) {
      divisor = 1000 * 1000;
    } else if (max > 1000) {
      divisor = 1000;
    } else if (max < 1000){
      divisor = 1000
    }
    let unit;
    if (units === "kilo") {
      if (data > 1000.0) {
        return {
          data: (data / 1000.0).toFixed(2),
          unit: "Tonnes"
        };
      } else {
        return {
          data: data.toFixed(2),
          unit: "Kilograms"
        };
      }
    } else if (units === "mps") {
      return {
        data: data.toFixed(2),
        unit: "watts per "
      };
    } else {
      unit = divisorToPrefix(divisor);
    }
    return {
      data: (data / divisor).toFixed(2),
      unit: unit
    };
  };
  
  export const convertTokW = data => {
    var max = data;
    var divisor = 1;
    if (max > 1000 * 1000 * 1000) {
      divisor = 1000 * 1000 * 1000;
      return max / divisor;
    } else if (max > 1000 * 1000) {
      divisor = 1000 * 1000;
      return max / divisor;
    } else if (max > 1000) {
      divisor = 1000;
      return max / divisor;
    } 
  };
  
  let divisorToPrefix = divisor => {
    if (divisor === 1) {
      return "watt";
    } else if (divisor === 1000) {
      return "kilowatt";
    } else if (divisor === 1000 * 1000) {
      return "Megawatt";
    } else if (divisor === 1000 * 1000 * 1000) {
      return "Gigawatt";
    }
  };
  
  export const colors = {
    default: "#344675",
    primary: "#42b883",
    info: "#1d8cf8",
    danger: "#fd5d93",
    teal: "#00d6b4",
    primaryGradient: [
      "rgba(76, 211, 150, 0.1)",
      "rgba(53, 183, 125, 0)",
      "rgba(119,52,169,0)"
    ]
  };
  
  export default {
    // baseUrl,
    convert,
    colors
  };